# CM Portal: A Django project for electronically storing data of the elderly residents of Camillus MedHaven Nursing Home 

## Installing and Running on Development Server

> The following contains instructions on how to install and run this project locally on your computer. Although, you may be able to install and run it on other Linux distributions and Windows as well, it is recommended that you use an Ubuntu machine to make things easier.

### Requirements
1. CM Portal requires **PostgreSQL** as its default database because it was designed to support multi-tenant apps using a shared database with isolated schema. Besides that, the project is hosted on **Heroku**, which is a Paas (Platform as a Service) cloud that provides **Heroku Postgres** DBaaS (Cloud database) as their main product, which is based on PostgreSQL. To install PostgresSQL on **Ubuntu 16.04** or higher version you can follow this simple [guide](https://www.digitalocean.com/community/tutorials/how-to-use-postgresql-with-your-django-application-on-ubuntu-16-04).

2. Also, this project makes use of **Dropbox** as its file system storage, so you have to create a **Dropbox app** specifically for this project. You can go to [Dropbox for Developers](https://www.dropbox.com/developers) to create one.

### Clone this project
`git clone https://gitlab.com/salvadorrye/cmportal-rev.git`

### Initialize local configuration and fetch all submodules data
`git submodule init`
`git submodule update`

### Set up a Python virtual environment
`cd cmportal-rev`
`virtualenv .venv -p /usr/bin/python3`
`source .venv/bin/activate`

### Install dependencies
`pip install -r requirements.txt`

### Create a secret key for your project
`django-admin.py startproject myproject`
`grep SECRET_KEY myproject/myproject/settings.py > mysecretkey.txt`

### Delete dummy files
`rm -rf myproject`

### Create a .env file inside the project root directory with the following contents 
~~~
SECRET_KEY=your-secret-key-here-from-mysecretkey.txt-without-the-single-quotes
DEBUG=True
ALLOWED_HOSTS=.localhost
DEFAULT_FILE_STORAGE=django.core.files.storage.FileSystemStorage
DROPBOX_ACCESS_TOKEN=your-dropbox-access-token
DROPBOX_CONSUMER_KEY=your-dropbox-consumer-key
DROPBOX_CONSUMER_SECRET=your-dropbox-consumer-secret
EMAIL_HOST=smtp.gmail.com
EMAIL_HOST_USER=your-gmail-username
EMAIL_HOST_PASSWORD=your-gmail-password
DEFAULT_FROM_EMAIL=Your Incredible Team <noreply@example.com>
DB_DEFAULT={"ENGINE": "django.db.backends.postgresql_psycopg2", "NAME": "your-database-name", "USER": "your-admin-username", "PASSWORD": "your-password", "HOST": "localhost", "PORT": ""}
TENANTS={"www.localhost": "public", "localhost": "public"}
~~~
**Note:** Replace values accordingly then delete mysecretkey.txt after.
`rm mysecretkey.txt`

### Authorize your Dropbox app
Run the following command and follow up on screen instructions: 
`python manage.py get_dropbox_token --settings=mysite.settings`

Finally edit `.env` file and add the following line replacing the value with the `DROPBOX_ACCESS_TOKEN_SECRET` given to you by **Dropbox**.
`DROPBOX_ACCESS_TOKEN_SECRET=your-dropbox-access-token-secret`

### Propagate models into the database
`./manage.py migrate`

### Create a super user
`./manage.py createsuperuser`

### Run local server
`./manage.py runserver`

## Deploying on Heroku
> To do.

