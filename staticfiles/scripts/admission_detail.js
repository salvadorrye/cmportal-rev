document.addEventListener('DOMContentLoaded', () => {
    const vTabs = document.querySelectorAll('#v-tabs-tab a.nav-link');
    const hTabs = document.querySelectorAll('ul.nav-tabs li.nav-item a');   
    const tabLists = document.querySelectorAll('ul.nav-tabs'); 
      
    /* When the browser is refreshed shows the saved active hTab 
    	     or a default one if there is none */
    	     
    tabLists.forEach(tablist => {
    	  // Selects the first one on the list and make it the active hTab
    	  if (!localStorage.getItem(tablist.id)) {
    	      let hTab = document.querySelectorAll(`#${tablist.id} li.nav-item a`)[0];
    	      let hContent = document.querySelector(hTab.getAttribute('href'));
    	      
    	      showActive(hTab, hContent);
    	  } else {
    	  	   let tab_id = localStorage.getItem(tablist.id);
    	  	   let hTab = document.querySelector(`#${tab_id}`);
    	  	   let hContent = document.querySelector(hTab.getAttribute('href'));
    	  	       	  	   
    	  	   showActive(hTab, hContent);
    	  }
    });  
    
    // Attach an event listener for each hTab to save it when clicked  
    hTabs.forEach(hTab => {	  
        let parentid = hTab.parentElement.parentElement.id;
        hTab.addEventListener('click', e => {            
            localStorage.setItem(parentid, e.target.id);
        });
    });
        	
    /* When the browser is refreshed shows the saved active vTab 
    	     or a default one if there is none */
	   
    if (!localStorage.getItem('vTab')) {
        // Selects the first one on the list and make it the active vTab
   	  let vTab = vTabs[0];
   	  let vContent = document.querySelector(vTab.getAttribute('href'));
   	
   	  showActive(vTab, vContent);
   	
    } else {   	               	
        let vTab = document.querySelector(`#${localStorage.getItem('vTab')}`);
        let vContent = document.querySelector(vTab.getAttribute('href'));
   	    	
        showActive(vTab, vContent);
    }	       
	 
	 // Attach an event listener for each vTab that saves it when clicked
    vTabs.forEach(vTab => {
        vTab.addEventListener('click', e => {
		      //console.log(e.target.id);
	         localStorage.setItem('vTab', e.target.id);	
        });	
    });
    
    function showActive(vtab, vcontent) {
        vtab.classList.add('active');   	   	
        vtab.setAttribute('aria-selected', 'true');
        
        vcontent.classList.add('show');
   	  vcontent.classList.add('active');   
    }	
});

