"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
#from django.contrib import admin
from baton.autodiscover import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from geriatrics.admin import geriatrics_admin_site
from central_supplies.admin import central_supplies_admin_site
from health_declaration.admin import health_declaration_admin_site
from medication.admin import medication_admin_site
from patient_monitoring.admin import patient_monitoring_admin_site
from plan_of_care.admin import plan_of_care_admin_site
from orders.models import Order

urlpatterns = [
    path('admin/', admin.site.urls),
    path('baton/', include('baton.urls')),
]

# Admin
urlpatterns += [
    path('geriatrics/admin/', geriatrics_admin_site.urls),
    path('medication/admin/', medication_admin_site.urls),
    path('patient_monitoring/admin/', patient_monitoring_admin_site.urls),
    path('plan_of_care/admin', plan_of_care_admin_site.urls),
    path('health_declaration/admin/', health_declaration_admin_site.urls),
    path('central_supplies/admin', central_supplies_admin_site.urls),
]

urlpatterns += [
    path('', include('cm_portal.urls')),
    path('account/', include('account.urls')),
    path('oauth/', include('social_django.urls', namespace='social')),
    path('geriatrics/', include('geriatrics.urls')),
    path('health_declaration/', include('health_declaration.urls')),
    #path('central_supplies/', include('central_supplies.urls')),
    #path('central-supplies/cart', include('cart.urls', namespace='cart')),
    #path('central-supplies/orders', include('orders.urls', namespace='orders')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = "Camillus MedHaven Admin"
admin.site.site_title = "Camillus MedHaven Admin Portal"
admin.site.index_title = "Welcome to Camillus MedHaven Portal"

